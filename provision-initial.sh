#!/bin/bash -e
#^ubuntu thing
export DEBIAN_FRONTEND=noninteractive # per https://serverfault.com/q/500764/309584
apt-get update -y -qq
# TODO: install display manager; currently have to run:
# export DISPLAY=:0.0 ; startx
# startkde

# MAYBE? DISPLAY=:0 && KWIN_COMPOSE=N kwin_x11 --replace& systemsettings & echo

# needs to match vboxnet0
cat << HEREDOC >> /etc/network/interfaces

# The host-only network interface
auto enp0s8
iface enp0s8 inet static
address 192.168.56.101
netmask 255.255.255.0
network 192.168.56.0
broadcast 192.168.56.255
HEREDOC

# region EnableProposed per EnableProposed
echo "deb http://archive.ubuntu.com/ubuntu/ artful-proposed restricted main multiverse universe" >> /etc/apt/sources.list

cat << HEREDOC >> /etc/apt/preferences.d/proposed-updates
Package: *
Pin: release a=artful-proposed
Pin-Priority: 400
HEREDOC
# endregion

# TODO test this part
# mv /home/ben/.ssh/id_rsa /home/ben/.ssh/id_rsa
# cat /home/ben/.ssh/id_rsa.pub >> /home/ben/.ssh/authorized_keys
echo "ben ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/ben

# vboxvideo has to be re-enabled to get the resolution right
echo "blacklist vboxvideo" >> /etc/modprobe.d/blacklist.conf
apt-get -y -qq -o=Dpkg::Use-Pty=0 install \
    linux-headers-$(uname -r) \
    build-essential \
    xorg \
    sddm \
    plasma-desktop \
    firefox \
    dolphin \
    konsole \
    terminator

add-apt-repository ppa:kubuntu-ppa/backports
apt -y -qq update && sudo apt -y -qq full-upgrade

# region install hyper
# per https://askubuntu.com/questions/902672/registering-appimage-files-as-a-desktop-app
wget -nv "https://github.com/AppImage/AppImageKit/releases/download/continuous/appimaged-x86_64.AppImage"
chmod +x ./appimaged-x86_64.AppImage
./appimaged-x86_64.AppImage --install

# also https://releases.hyper.is/download/AppImage
# need to link my config into here
wget -nv "https://github.com/zeit/hyper/releases/download/1.4.8/hyper-1.4.8-x86_64.AppImage"
chmod +x ./hyper-1.4.8-x86_64.AppImage
mv ./hyper-1.4.8-x86_64.AppImage ./hyper
mkdir -p ~/bin
mv ./hyper ~/bin
chown -R ben:ben ~
# endregion install hyper

# see https://stackoverflow.com/a/22723681/4200039
echo "10.0.2.2    localhost" >> "/etc/hosts"
echo "192.168.56.1    www.brightideadev.com" >> "/etc/hosts"
echo "192.168.56.1    mysqlagain1.brightideadev.com" >> "/etc/hosts"

# more packages
# i3 per https://i3wm.org/docs/repositories.html
# try installing the basic repo...
# /usr/lib/apt/apt-helper download-file http://debian.sur5r.net/i3/pool/main/s/sur5r-keyring/sur5r-keyring_2018.01.30_all.deb keyring.deb SHA256:baa43dbbd7232ea2b5444cae238d53bebb9d34601cc000e82f11111b1889078a
# dpkg -i ./keyring.deb
# echo "deb http://debian.sur5r.net/i3/ $(grep '^DISTRIB_CODENAME=' /etc/lsb-release | cut -f2 -d=) universe" >> /etc/apt/sources.list.d/sur5r-i3.list
apt -y update
apt -y install i3

# for desktop background image - see Xsession
apt -y install feh
apt -y install neovim
apt -y install fasd
apt -y install direnv
apt -y install xdotool

# pyenv
curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash

# maybe shutdown -r?
reboot
# modprobe -r vboxguest
# sed -i '$ d' /etc/modprobe.d/blacklist.conf

# wget -nv "https://www.virtualbox.org/download/testcase/VBoxGuestAdditions_5.2.7-120528.iso"
# mkdir /media/GuestAdditionsISO
# mount -o loop ./VBoxGuestAdditions_5.2.7-120528.iso /media/GuestAdditionsISO
# cd /media/GuestAdditionsISO
# ./VBoxLinuxAdditions.run install

# gpasswd -a ben vboxsf

# may have to reboot to do anything after this
# systemctl start NetworkManager # per https://bugs.launchpad.net/ubuntu/+source/network-manager/+bug/1718558/comments/4
# apt-get -y install ubuntu-desktop

# wget --no-check-certificate -O pub_key 'https://github.com/mitchellh/vagrant/raw/master/keys/vagrant.pub'
# chown ubuntu:ubuntu pub_key
# cat pub_key >> /home/ubuntu/.ssh/authorized_keys
# cat id_rsa.pub >> /home/ubuntu/.ssh/authorized_keys