### 2017 link dump
https://www.reddit.com/r/unixporn/comments/64mihc/i3_kde_plasma_a_match_made_in_heaven/ | [i3 + KDE Plasma] A match made in heaven : unixporn
https://blogs.gnome.org/mcatanzaro/2017/02/19/on-problems-with-vala/ | On Problems with Vala – Michael Catanzaro
https://apple.stackexchange.com/questions/234410/lshw-or-lspci-for-mac-os-x | software recommendation - lshw or lspci for Mac OS X - Ask Different
https://news.ycombinator.com/item?id=15339318 | Re: touchpad scrolling, make sure you disable "smooth" (animated) scrolling and ... | Hacker News
https://askubuntu.com/questions/321924/how-do-i-configure-a-minimal-desktop-environment | How do I configure a minimal desktop environment? - Ask Ubuntu
https://askubuntu.com/questions/873529/how-to-install-lxqt-on-ubuntu-server-16-04 | command line - how to install lxqt on ubuntu server 16.04 - Ask Ubuntu
https://askubuntu.com/questions/912621/failed-to-start-session-for-main-and-guest-accounts | login - "Failed to start session" for main and guest accounts - Ask Ubuntu
https://linuxhint.com/install-kde-plasma-ubuntu/ | How to install KDE Plasma 5.10.2 on Ubuntu 17.04 | Linux Hint
https://www.ostechnix.com/list-installed-packages-certain-repository-linux/ | How To List Installed Packages From A Certain Repository In Linux - OSTechNix
https://wiki.qemu.org/Hosts/Mac | Hosts/Mac - QEMU
https://github.com/swaywm/sway/wiki/Unsupported-packages | Unsupported packages · swaywm/sway Wiki
https://unix.stackexchange.com/questions/146784/is-grub-the-best-bootloading-solution-is-there-an-easier-alternative | grub2 - Is GRUB the best bootloading solution? Is there an easier alternative? - Unix & Linux Stack Exchange
https://askubuntu.com/questions/32499/migrate-from-a-virtual-machine-vm-to-a-physical-system | virtualization - Migrate from a virtual machine (VM) to a physical system - Ask Ubuntu
https://superuser.com/questions/114445/is-it-possible-to-convert-virtual-machines-to-physical-environments | virtualbox - Is it possible to convert virtual machines to physical environments? - Super User
https://stackoverflow.com/questions/21920993/convert-amazon-ec2-ami-to-virtual-or-vagrant-box | virtualbox - Convert Amazon EC2 AMI to Virtual or Vagrant box - Stack Overflow
https://askubuntu.com/questions/16042/how-to-get-to-the-grub-menu-at-boot-time | grub2 - How to get to the GRUB menu at boot-time? - Ask Ubuntu
https://askubuntu.com/questions/305229/whats-the-best-way-to-ssh-to-machines-on-the-local-network | networking - What's the best way to SSH to machines on the local network? - Ask Ubuntu
